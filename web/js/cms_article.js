$(function() {


    // Выбор ктегории статьи
    $(document.body).on('change', '.cms_article_list_category_selector select', function() {


        var idx = $(this).val();
        var start_url = $('.cms_article_list_category_selector').attr('start_url');

        window.location.href = start_url + idx;

        return false;
    });



    // Удаление статьи
    $(document.body).on('click', '.cms_article_set_deleted', function() {
        var idx = $(this).attr('idx');
        var deleted = Number($(this).attr('deleted'));

        var confirm_text = 'Пометить статью #' + idx + ' как удаленную?';
        if (deleted == 1) {
            confirm_text = 'Удалить статью #' + idx + ' безвозвратно';
        }

        if (confirm(confirm_text)) {

            return true;
        }

        return false;
    });






    // Вернуть статью из корзины
    $(document.body).on('click', '.cms_article_restore_from_trash', function() {

        var idx = $(this).attr('idx');
        var confirm_text = 'Вернуть статью #' + idx + ' из корзины?';

        if (confirm(confirm_text)) {

            return true;
        }

        return false;
    });

});
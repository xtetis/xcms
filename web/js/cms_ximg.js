function readURL(input, previewId) {
    console.log(input.files);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            console.log('e');
            console.log(e);
            $(previewId).attr('src', e.target.result);
            $(previewId).hide();
            $(previewId).fadeIn(650);
        }
        console.log(input.files[0]);
        reader.readAsDataURL(input.files[0]);
    }
}

class xms_img {
    static deleteImage(id, url_delete_image, gallery_id) {
        if (confirm('Удалить изображение ?')) {
            $.post(url_delete_image, {
                    id: id
                }, function(obj) {


                    console.log('obj.result = ' + obj.result);
                    if (obj.result) {
                        $(".gallery_" + gallery_id + "_container").load(window.location.href + " .gallery_" + gallery_id + "_container_inner");
                    } else {
                        xform.prepareErrors(obj, obj);
                    }

                },
                'json');
        }
    }
}


$(function() {


    $('#jstree_gallery').on('changed.jstree', function(e, data) {
        var i, j = [];
        for (i = 0, j = data.selected.length; i < j; i++) {
            var url__cms_galleries_list = $('#url__cms_galleries_list').val();
            var id = data.instance.get_node(data.selected[i]).id;
            var idx = $('#' + id).attr('idx');
            $(".gallery_container").load(url__cms_galleries_list + "?id_gallery=" + idx + " .gallery_container_inner");
            break;
        }
    }).jstree();



    $(document.body).on('change', '#inputGroupFile04', function() {
        readURL(this, '#img_prev');
    });



    $(document.body).on('click', '.btn_cms_delete_image', function() {
        var idx = $(this).attr('idx');
        var url_delete_image = $(this).attr('url_delete_image');
        var id_gallery = $(this).attr('id_gallery');
        xms_img.deleteImage(idx, url_delete_image, id_gallery);
    });


});
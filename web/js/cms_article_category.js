$(function() {


    $('#jstree_article_category').on('changed.jstree', function(e, data) {
        var i, j = [];

        $('.mod_cms_article_category_info_block').hide();

        for (i = 0, j = data.selected.length; i < j; i++) {
            var id = data.instance.get_node(data.selected[i]).id;
            var idx = $('#' + id).attr('idx');
            var name = $('#' + id).attr('name');

            $('.mod_cms_article_category_info_block a').show();

            $('.mod_cms_article_category_info_block .article_category_name').html(name);

            var edit_href_start = $('.mod_cms_article_category_info_block .btn_article_category_edit_link').attr('href_start');
            var delete_href_start = $('.mod_cms_article_category_info_block .btn_article_category_delete_link').attr('href_start');

            $('.mod_cms_article_category_info_block .btn_article_category_edit_link').attr('href', edit_href_start + idx);
            $('.mod_cms_article_category_info_block .btn_article_category_delete_link').attr('href', delete_href_start + idx);

            $('.mod_cms_article_category_info_block').show('slow');
            break;
        }

    }).jstree({
        "core": {
            "multiple": false,
            "animation": 0
        }
    });



});


class cms_gen_anket {
    /**
     * Генерирует картинку профиля
     */
    static generateImg() {
        xform.readUrlToImageNoalbum(
            $('.xform_input_image_noalbum'), 
            'https://thispersondoesnotexist.com/'
        );
    }

    /**
     * Генерирует случайные данные пользователя
     */
    static generateProfile() {
        $('#gen_profile_data').submit();
    }

    /**
     * Обрабатывает данные полученные при генерации данных пользователя
     */
    static prepareGenerateUserData() {
        $.each(xform_response.data, function(i, item) {
            $('input[name="'+i+'"]').val(item);
            $('textarea[name="'+i+'"]').val(item);
            $('select[name="'+i+'"]').val(item);
        });
        $.each(xform_response.text_value, function(i, item) {
            $('input.'+i).val(item);
        });
    }
}


$(function() {

    $(document.body).on('click', '#gen_photo', function() {
        cms_gen_anket.generateImg();
    });

    $(document.body).on('click', '#gen_profile_data', function() {
        cms_gen_anket.generateProfile();
    });
    
    
});
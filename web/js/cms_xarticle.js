$(function() {

    // Удаление статьи
    $(document.body).on('click', '.cms_article_set_deleted', function() {


        var idx = $(this).attr('idx');
        var url__article_set_deleted = $('#url__article_set_deleted').val();
        var url__cms_article_list = $('#url__cms_article_list').val();
        var deleted = Number($(this).attr('deleted'));

        var confirm_text = 'Пометить статью как удаленную?';
        if (deleted == 1) {
            confirm_text = 'Удалить статью безвозвратно';
        }

        if (confirm(confirm_text)) {
            var url__article_set_deleted_full = url__article_set_deleted + '?id=' + idx;

            $.get(url__article_set_deleted_full, function(data) {
                $(".table_article_list_container").load(url__cms_article_list + " .table_article_list_inner_container");
            });
        }

        return false;
    });


    // Вернуть статью из корзины
    $(document.body).on('click', '.cms_article_restore_from_trash', function() {

        var idx = $(this).attr('idx');
        var url__article_restore_from_trash = $('#url__article_restore_from_trash').val();
        var url__cms_article_list = $('#url__cms_article_list').val();

        var confirm_text = 'Вернуть статью из корзины?';

        if (confirm(confirm_text)) {
            var url__article_restore_from_trash_full = url__article_restore_from_trash + '?id=' + idx;

            $.get(url__article_restore_from_trash_full, function(data) {
                $(".table_article_list_container").load(url__cms_article_list + " .table_article_list_inner_container");
            });
        }

        return false;
    });

});
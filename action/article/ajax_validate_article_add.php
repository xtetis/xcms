<?php

/**
 * Валидация при добавлении статьи
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

$id_article       = \xtetis\xengine\helpers\RequestHelper::post('id_article', 'int', 0);
$id_category_list = $_POST['id_category_list'] ?? [];
$name             = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');
$introtext        = \xtetis\xengine\helpers\RequestHelper::post('introtext', 'raw', '');
$about            = \xtetis\xengine\helpers\RequestHelper::post('about', 'raw', '');

if (!is_array($id_category_list))
{
    $id_category_list = [];
}

foreach ($id_category_list as &$item)
{
    $item = intval($item);
}

$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$params = [
    'name'             => $name,
    'about'            => $about,
    'introtext'        => $introtext,
    'id'               => $id_article,
    'id_category_list' => $id_category_list,
];

$article_model = new \xtetis\xarticle\models\ArticleModel($params);
if ($article_model->saveArticle())
{

    $url_article_edit = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'article_edit',
        ],
        'query' => [
            'id' => $article_model->id,
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $url_article_edit;
}
else
{
    $response['errors'] = $article_model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

<?php

/**
 * Восстанавливает из корзины статью
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$article_model = \xtetis\xarticle\models\ArticleModel::generateModelById($id);
if (!$article_model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Статья #'.$id.' не найдена');
}

if ($article_model->deleted)
{
    // Помечаем статью как не удаленную (убираем из корзину)
    $article_model->deleted = 0;
    $article_model->setDeleted();
}

if ($article_model->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($article_model->getLastErrorMessage());
}

$url_article_list = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'article_list',
    ],
]);

header('Location: ' . $url_article_list);
exit;

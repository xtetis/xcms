<?php

/**
 * Рендер формы редактирования статьи
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model_article = \xtetis\xarticle\models\ArticleModel::generateModelById($id);
if (!$model_article)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Статья не найдена');
}

// Урлы
// ********************************************
$urls = [];
$urls['url_validate_form'] = self::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_article_edit',
    ],
]);

$urls['url_article_list'] = self::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'article_list',
    ],
]);

$urls['url_cms_main'] = self::makeUrl();
// ********************************************


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_article'     => $model_article,
    'urls'  => $urls,
]);

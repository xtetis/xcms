<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$url_cms_main = \xtetis\xcms\Component::makeUrl();

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model_article_category = new \xtetis\xarticle\models\ArticleCategoryModel([
    'id' => $id,
]);

$model_article_category->getById();

$model_root_article_category = new \xtetis\xarticle\models\ArticleCategoryModel();

$url_validate_form = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_edit_category',
    ],
]);

$url_category_list = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'category_list',
    ],
]);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_root_article_category' => $model_root_article_category,
        'model_article_category'      => $model_article_category,
        'url_validate_form'           => $url_validate_form,
        'url_category_list'           => $url_category_list,
        'url_cms_main'                => $url_cms_main,
    ],
);

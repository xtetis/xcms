<?php

/**
 * Рендер списка статей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$model_article_anyfield_component = new \xtetis\xarticle\models\ArticleAnyfieldComponentModel();
$model_article_anyfield_component->getModelList();


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_article_anyfield_component'             => $model_article_anyfield_component,
    ],
);

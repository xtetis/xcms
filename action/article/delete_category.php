<?php

/**
 * Удаляем категорию
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model_article_category = new \xtetis\xarticle\models\ArticleCategoryModel(
    [
        'id' => $id,
    ]
);

$model_article_category->deleteCategory();

$url_category_list = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'category_list',
    ],
]);

header('Location: ' . $url_category_list);
exit;

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$model_root_article_category = new \xtetis\xarticle\models\ArticleCategoryModel(
    [
        'id_parent' => 0,
    ]
);

$model_root_article_category->getChildModelList(true);

$url_delete_category_start = \xtetis\xcms\Component::makeUrl([
    'path'  => [
        \xtetis\xengine\App::getApp()->getAction(),
        'delete_category',
    ],
    'query' => [
        'id' => '',
    ],
]);

$url_edit_category_start = \xtetis\xcms\Component::makeUrl([
    'path'  => [
        \xtetis\xengine\App::getApp()->getAction(),
        'edit_category',
    ],
    'query' => [
        'id' => '',
    ],
]);

$url_add_category = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'add_category',
    ],
]);

$url_cms_main = \xtetis\xcms\Component::makeUrl();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_root_article_category' => $model_root_article_category,
        'url_delete_category_start'   => $url_delete_category_start,
        'url_edit_category_start'     => $url_edit_category_start,
        'url_add_category'            => $url_add_category,
        'url_cms_main'                => $url_cms_main,
    ],
);

<?php

/**
 * Рендер списка статей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id_category = \xtetis\xengine\helpers\RequestHelper::get('id_category', 'int', 0);
$page        = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);

$model_root_article_category = new \xtetis\xarticle\models\ArticleCategoryModel();

$id_category_list = [];
if ($id_category)
{
    $id_category_list = [$id_category];
}

$url_article_list = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'article_list',
    ],
]);

$url_start_article_list = \xtetis\xcms\Component::makeUrl([
    'path'  => [
        \xtetis\xengine\App::getApp()->getAction(),
        'article_list',
    ],
    'query' => [
        'id_category' => '',
    ],
]);

$url_add_article = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'article_add',
    ],
]);

$params = [
    'id_category_list' => $id_category_list,
    'current_page'     => $page,
    'deleted'          => 1,
];

$model_article_list = new \xtetis\xarticle\models\ArticleListModel(
    $params
);

$model_article_list->getModelList();

if ($model_article_list->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_article_list->getLastErrorMessage());
}

$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    10,
    $page,
    $model_article_list->count_articles,
    $url_start_article_list
);

$url_article_delete_link_list   = [];
$url_article_undelete_link_list = [];
$url_article_edit_link_list     = [];
foreach ($model_article_list->article_model_list as $id_article => $model_article)
{
    $url_article_delete_link_list[$id_article] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'article_delete',
        ],
        'query' => [
            'id' => $id_article,
        ],
    ]);

    $url_article_undelete_link_list[$id_article] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'article_undelete',
        ],
        'query' => [
            'id' => $id_article,
        ],
    ]);

    $url_article_edit_link_list[$id_article] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'article_edit',
        ],
        'query' => [
            'id' => $id_article,
        ],
    ]);

}

$url_cms_main = \xtetis\xcms\Component::makeUrl();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_article_list'             => $model_article_list,
        //'model_article_category' => $model_article_category,

        //'articles_list_info'               => $articles_list_info,
        'model_root_article_category'    => $model_root_article_category,
        'id_category'                    => $id_category,
        'url_article_list'               => $url_article_list,
        'url_start_article_list'         => $url_start_article_list,
        'url_add_article'                => $url_add_article,
        'url_article_edit_link_list'     => $url_article_edit_link_list,
        'url_article_delete_link_list'   => $url_article_delete_link_list,
        'url_article_undelete_link_list' => $url_article_undelete_link_list,
        'url_cms_main'                   => $url_cms_main,
        'pagination'                     => $pagination,
    ],
);

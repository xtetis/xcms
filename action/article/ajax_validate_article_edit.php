<?php

/**
 * Валидация при редактировании статьи
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

require __DIR__.'/ajax_validate_article_add.php';
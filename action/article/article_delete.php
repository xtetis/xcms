<?php

/**
 * Устанавливаем признак "Удалена" для xarticle
 * Или если статья уже удалена - удаляет запись из БД
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$article_model = new \xtetis\xarticle\models\ArticleModel(
    [
        'id' => $id,
    ]
);
$article_model->getById();
if (!$article_model->deleted)
{
    // Помечаем статью как удаленную (помещаем в корзину)
    $article_model->deleted = 1;
    $article_model->setDeleted();
}
else
{
    // Удаляем запись из БД
    $article_model->deleteArticle();
}

if ($article_model->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($article_model->getLastErrorMessage());
}

$url_article_list = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'article_list',
    ],
]);

header('Location: ' . $url_article_list);
exit;

<?php

/**
 * Рендер формы добавления статьи
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$model_article = new \xtetis\xarticle\models\ArticleModel();


// Урлы
// --------------------------------------------------------------
$urls['url_article_list'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'article_list',
    ],
]);

$urls['url_validate_form'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_article_add',
    ],
]);

$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
// --------------------------------------------------------------




// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_article'     => $model_article,
    'urls'     => $urls,
]);

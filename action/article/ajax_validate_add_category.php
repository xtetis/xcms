<?php

/**
 * Валидация при добавлении категории
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$id_parent = \xtetis\xengine\helpers\RequestHelper::post('id_parent', 'int', 0);
$name      = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');

$model_article_category = new \xtetis\xarticle\models\ArticleCategoryModel(
    [
        'name'      => $name,
        'id_parent' => $id_parent,
    ]
);

$model_article_category->addCategory();

if (!$model_article_category->getErrors())
{

    $url_category_list = \xtetis\xcms\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'category_list',
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $url_category_list;
}
else
{
    $response['errors'] = $model_article_category->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

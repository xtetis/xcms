<?php

/**
 * Валидация при пункта меню
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$id        = \xtetis\xengine\helpers\RequestHelper::post('id', 'int', '');
$name      = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');
$type      = \xtetis\xengine\helpers\RequestHelper::post('type', 'int', '');
$content   = \xtetis\xengine\helpers\RequestHelper::post('content', 'str', '');
$id_parent = \xtetis\xengine\helpers\RequestHelper::post('id_parent', 'int', '');

$model = new \xtetis\xcms\models\MenuModel(
    [
        'id'        => $id,
        'name'      => $name,
        'type'      => $type,
        'content'   => $content,
        'id_parent' => $id_parent,
    ]
);

if ($model->editMenu())
{

    $go_to_url = \xtetis\xcms\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'menu',
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

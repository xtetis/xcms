<?php

/**
 * Рендер списка пунктов меню
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$list_model_params = \xtetis\xcms\models\MenuModel::getListModelsParams([
    'limit' => 999999,
]);

// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
$urls['url_add_menu'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'menu_add',
    ],
]);
foreach ($list_model_params['models'] as $id => $model_category)
{
    $urls['url_edit'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'menu_edit',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['url_delete'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'menu_delete',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
}
// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'list_model_params' => $list_model_params,
        'urls'              => $urls,
    ],
);

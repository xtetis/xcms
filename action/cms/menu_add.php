<?php

/**
 * Рендер списка пунктов меню
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
$urls['url_menu'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'menu',
    ],
]);
$urls['url_validate_menu_add'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_menu_add',
    ],
]);

// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'              => $urls,
    ],
);

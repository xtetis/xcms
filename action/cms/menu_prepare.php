<?php

/**
 * Рендер списка пунктов меню
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model = \xtetis\xcms\models\MenuModel::generateModelById($id);
if (!$model) 
{
    \xtetis\xengine\helpers\LogHelper::customDie('Пункт меню не найден');
}



// Если ссылка = обрабатываемый файл PHP
if (intval($model->type))
{
    
    $filename = ENGINE_DIRECTORY.$model->content;
    if (!file_exists($filename))
    {
        \xtetis\xengine\helpers\LogHelper::customDie('Файл не существует');
    }

    $file_content = file_get_contents($filename);

    if (strpos($file_content, "(!defined('SYSTEM'))") === false)
    {
        \xtetis\xengine\helpers\LogHelper::customDie('В исполняемом файле не найдено определение SYSTEM');
    }
}
else
{
    $url = $model->content;
    header("Location: ".$url);
    exit;

}

// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
// ------------------------------------------------

$content = \xtetis\xengine\helpers\RenderHelper::renderFile($filename);


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'  => $urls,
        'content' => $content,
    ],
);

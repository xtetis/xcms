<?php
/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');


$model_root_gallery = new \xtetis\ximg\models\GalleryModel(
    [
        'id_parent' => 0,
    ]
);

$model_root_gallery->getChildModelList(true);


$url_cms_main = \xtetis\xcms\Component::makeUrl();
$url_cms_gallery = \xtetis\xcms\Component::makeUrl([
    'path'=>[
        \xtetis\xengine\App::getApp()->getAction(),
    ]
]);


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_root_gallery' => $model_root_gallery,
        'url_cms_main'=>$url_cms_main,
        'url_cms_gallery'=>$url_cms_gallery,
    ],
);

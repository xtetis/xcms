<?php

/**
 * Удаляет изображение
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id = \xtetis\xengine\helpers\RequestHelper::post('id', 'int', 0);

$response['result'] = false;
$response['errors'] = [];

$model_img = \xtetis\ximg\models\ImgModel::generateModelById($id);
if (!$model_img)
{

    $response['errors']['common'] = 'Изображение не найдено';
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;

}

// Удаляе изображение
if (!$model_img->deleteImage())
{
    $response['errors']['common'] = $model_img->getLastErrorMessage();
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
} 

$response['result'] = true;

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

<?php

/**
 * Загрузка изображения в галерею
 *      POST
 *          id_gallery
 *          type
 *          filedata
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

$response['js_fail'] = '';

$id_gallery = \xtetis\xengine\helpers\RequestHelper::post('id_gallery', 'int', 0);
$type       = \xtetis\xengine\helpers\RequestHelper::post('type', 'str', '');
if ('base64' == $type)
{
    $filedata = \xtetis\xengine\helpers\RequestHelper::post('filedata', 'raw', '');
}

$response['js_success'] = '$(".gallery_' . $id_gallery . '_container").load(window.location.href + " .gallery_' . $id_gallery . '_container_inner");';

$img_upload_model = new \xtetis\ximg\models\ImgUploadModel([
    'id_gallery' => $id_gallery,
    'type'       => $type,
    'filedata'   => $filedata,
]);
if (!$img_upload_model->uploadFile())
{
    $response['errors']['common'] = $img_upload_model->getLastErrorMessage();

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$img_model = new \xtetis\ximg\models\ImgModel([
    'id_gallery' => $id_gallery,
    'filename'   => $img_upload_model->upload_full_path,
]);
if (!$img_model->addImgFromFile())
{

    @unlink($img_upload_model->upload_full_path);

    $response['errors']['common'] = $img_model->getLastErrorMessage();

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$response['result'] = true;

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

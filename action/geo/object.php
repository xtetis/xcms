<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$only_with_date_ankets = \xtetis\xengine\helpers\RequestHelper::get('only_with_date_ankets', 'int', 0);

$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);

$limit  = 20;
$offset = ($page - 1) * $limit;

if ($only_with_date_ankets)
{
    $model_list = \xtetis\xgeo\models\ObjectModel::getListModelsParams([
        'offset' => $offset,
        'order'  => 'id DESC',
        'where'  => [
            'id IN (SELECT DISTINCT `id_geo_object` FROM `xdate_profile`)',
        ],
    ]);
}
else
{
    $model_list = \xtetis\xgeo\models\ObjectModel::getListModelsParams([
        'offset' => $offset,
        'order'  => 'id DESC',
    ]);
}

// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_start_list_only_nwda'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'object',
    ],
]);
$urls['url_start_list_only_wda'] = \xtetis\xcms\Component::makeUrl([
    'path'  => [
        \xtetis\xengine\App::getApp()->getAction(),
        'object',
    ],
    'query' => [
        'only_with_date_ankets' => 1,
    ],
]);

$urls['url_start_list'] = $urls['url_start_list_only_nwda'];

if ($only_with_date_ankets)
{
    $urls['url_start_list'] = $urls['url_start_list_only_wda'];
}

foreach ($model_list['models'] as $id => $model)
{
    $query = [
        'ret_to_page' => $page,
    ];
    if ($only_with_date_ankets)
    {
        $query = [
            'ret_to_page'           => $page,
            'only_with_date_ankets' => 1,
        ];
    }
    $urls['url_translit'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'translit_geo',
            $id,
        ],
        'query' => $query,
    ]);
}
// --------------------------------------------

$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $limit,
    $page,
    $model_list['count'],
    $urls['url_start_list']
);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'                  => $urls,
        'model_list'            => $model_list,
        'pagination'            => $pagination,
        'only_with_date_ankets' => $only_with_date_ankets,
    ],
);

<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$only_with_date_ankets = \xtetis\xengine\helpers\RequestHelper::get('only_with_date_ankets', 'int', 0);
$ret_to_page = \xtetis\xengine\helpers\RequestHelper::get('ret_to_page', 'int', 1);

$model = \xtetis\xgeo\models\ObjectModel::generateModelById($id);
if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Гео объект не найден');
}

if (!$model->generateSef())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model->getLastErrorMessage());
}

$query = [];
if ($only_with_date_ankets)
{
    $query['only_with_date_ankets']=1;
}
if ($ret_to_page>1)
{
    $query['p']=$ret_to_page;
}

$url = \xtetis\xcms\Component::makeUrl([
    'path'=>[
        \xtetis\xengine\App::getApp()->getAction(),
        'object'
    ],
    'query'=>$query
]);

header("Location: ".$url);
exit();
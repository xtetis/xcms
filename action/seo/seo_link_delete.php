<?php


// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model = \xtetis\xseo\models\SeoLinkModel::generateModelById($id);
if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найдена запись');
}

if (!$model->deleteSeo())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model->getLastErrorMessage());
}



$urls['url_seo'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'index',
    ],
]);
// --------------------------------------------


header("Location: ".$urls['url_seo']);
exit();
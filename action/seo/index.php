<?php

/**
 * Рендер списка пунктов меню
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');


$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);

$limit  = 20;
$offset = ($page - 1) * $limit;

$model_list = \xtetis\xseo\models\SeoPageModel::getListModelsParams([
    'offset' => $offset,
    'order'  => 'id DESC',
]);

// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
$urls['url_add_seo'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'seo_add',
    ],
]);
$urls['url_add_seo_link'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'seo_link_add',
    ],
]);
$urls['url_start_list'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'index',
    ],
]);

foreach ($model_list['models'] as $id => $model_seo_page)
{
    
    $urls['url_edit'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'seo_edit',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['url_delete'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'seo_delete',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['url_unactivate'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'seo_set_active',
        ],
        'query' => [
            'id' => $id,
            'active'=>0
        ],
    ]);
    $urls['url_activate'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'seo_set_active',
        ],
        'query' => [
            'id' => $id,
            'active'=>1
        ],
    ]);

    foreach ($model_seo_page->getSeoLinkModels() as $model_seo_link) 
    {
        $urls['url_seo_link_edit'][$model_seo_link->id] = \xtetis\xcms\Component::makeUrl([
            'path'  => [
                \xtetis\xengine\App::getApp()->getAction(),
                'seo_link_edit',
            ],
            'query' => [
                'id' => $model_seo_link->id,
            ],
        ]);
        $urls['url_seo_link_delete'][$model_seo_link->id] = \xtetis\xcms\Component::makeUrl([
            'path'  => [
                \xtetis\xengine\App::getApp()->getAction(),
                'seo_link_delete',
            ],
            'query' => [
                'id' => $model_seo_link->id,
            ],
        ]);
    }


    
}
// ------------------------------------------------


$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $limit,
    $page,
    $model_list['count'],
    $urls['url_start_list']
);


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_list' => $model_list,
        'urls'              => $urls,
        'pagination' => $pagination,
    ],
);

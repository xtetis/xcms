<?php

/**
 * Валидация при добавлении группы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$url      = \xtetis\xengine\helpers\RequestHelper::post('url', 'str', '');
$title       = \xtetis\xengine\helpers\RequestHelper::post('title', 'str', '');
$description = \xtetis\xengine\helpers\RequestHelper::post('description', 'str', '');
$hone        = \xtetis\xengine\helpers\RequestHelper::post('hone', 'str', '');
$name        = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');
$seotext     = \xtetis\xengine\helpers\RequestHelper::post('seotext', 'raw', '');

$model = new \xtetis\xseo\models\SeoPageModel();

// Загружает данные из POST параметров
$model->loadPostData();

if ($model->addSeo())
{

    $go_to_url = \xtetis\xcms\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'index',
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

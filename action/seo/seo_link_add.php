<?php

/**
 * Добавляет ссылку на продвигаемую страницу
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');


// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_validate'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_add_seo_link',
    ],
]);

$urls['url_seo'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'index',
    ],
]);
// --------------------------------------------

$model = new \xtetis\xseo\models\SeoLinkModel();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'=>$urls,
        'model' => $model,
    ],
);

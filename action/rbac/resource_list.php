<?php

/**
 * Рендер списка статей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$model_rbac_resource = new \xtetis\xrbac\models\RbacResourceModel([
    'id'=>11
]);

$model_rbac_resource->getModelById();
print_r($model_rbac_resource);

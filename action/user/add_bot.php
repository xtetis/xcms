<?php

/**
 * Рендер списка ботов
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}
\xtetis\xengine\App::getApp()->setParam('layout', 'manager');


$sex = rand(0,1);


if ($sex)
{
    $filename_names = __DIR__.'/../../data/bot_names/ru/male_names.txt';
}
else
{
    $filename_names = __DIR__.'/../../data/bot_names/ru/female_names.txt';
}

if (!file_exists( $filename_names))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найден файл с именами');
}

$names = file_get_contents($filename_names);
$names = explode("\n",$names);
$rand_index = array_rand($names, 1);
$name = $names[$rand_index];

$model_user_bot = new \xtetis\xuser\models\UserBotModel(
    [
        'name'=>$name,
    ]
);
if (!$model_user_bot->generateBot())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_user_bot->getLastErrorMessage());
}


$url_bots = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'bot',
    ],
]);



header("Location: ".$url_bots);
exit;

<?php

/**
 * Рендер списка ботов
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}
\xtetis\xengine\App::getApp()->setParam('layout', 'manager');


$id = \xtetis\xengine\helpers\RequestHelper::get('id','int',0);

$model_bot = \xtetis\xuser\models\UserBotModel::generateModelById($id);
if (!$model_bot)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найден бот');
}

$model_bot->generateHash();



$url_bots = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'bot',
    ],
]);



header("Location: ".$url_bots);
exit;

<?php

/**
 * Рендер списка ботов
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);

$limit  = 20;
$offset = ($page - 1) * $limit;



$model_list = \xtetis\xuser\models\UserBotModel::getListModelsParams([
    'offset' => $offset,
    'order'  => 'id DESC',
]);


// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
$urls['url_add_bot']  = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'add_bot',
    ],
]);

foreach ($model_list['models'] as $id => $model_bot)
{
    $urls['url_regenerate_access_hash'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'regenerate_access_hash_bot',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
}

$urls['url_start_list'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'bot',
    ],
]);

// ------------------------------------------------


$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $limit,
    $page,
    $model_list['count'],
    $urls['url_start_list']
);



// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'           => $urls,
        'model_list' => $model_list,
        'pagination' => $pagination,
    ],
);

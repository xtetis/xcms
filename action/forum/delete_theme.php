<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id','int',0);


$model = \xtetis\xforum\models\ThemeModel::generateModelById($id);
if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найдена тема');
}

if (!$model->deleteTheme())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model->getLastErrorMessage());
}


// --------------------------------------------
$urls['url_theme'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'theme',
    ],
]);
// --------------------------------------------

header("Location: ".$urls['url_theme']);
exit;

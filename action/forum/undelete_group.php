<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id','int',0);


$model_group = \xtetis\xforum\models\GroupModel::generateModelById($id);
if (!$model_group)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найдена группа');
}

if (!$model_group->undeleteGroup())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_group->getLastErrorMessage());
}


// --------------------------------------------
$urls['url_group'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'group',
    ],
]);
// --------------------------------------------

header("Location: ".$urls['url_group']);
exit;

<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');


// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_validate_add_group'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_add_group',
    ],
]);

$urls['url_group'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'group',
    ],
]);
// --------------------------------------------



// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'=>$urls,
    ],
);

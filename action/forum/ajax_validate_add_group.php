<?php

/**
 * Валидация при добавлении группы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$name      = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');
$id_parent = \xtetis\xengine\helpers\RequestHelper::post('id_parent', 'int', '');
$about     = \xtetis\xengine\helpers\RequestHelper::post('about', 'str', '');
$image     = $_POST['image'] ?? [];

$model_date_group = new \xtetis\xforum\models\GroupModel(
    [
        'name'      => $name,
        'id_parent' => $id_parent,
        'about'     => $about,
        'image'     => $image,

    ]
);

if ($model_date_group->addGroup())
{

    $go_to_url = \xtetis\xcms\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'group',
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model_date_group->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

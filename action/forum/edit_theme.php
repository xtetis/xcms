<?php

/**
 * Контроллер формы редактироваания темы форума
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model = \xtetis\xforum\models\ThemeModel::generateModelById($id);
if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найдена тема');
}

// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_validate_edit_theme'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_edit_theme',
    ],
]);

$urls['url_theme'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'theme',
    ],
]);
// --------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model' => $model,
        'urls'  => $urls,
    ],
);

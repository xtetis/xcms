<?php

/**
 * Валидация при редактирования группы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$id        = \xtetis\xengine\helpers\RequestHelper::post('id', 'int', 0);
$name      = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');
$id_parent = \xtetis\xengine\helpers\RequestHelper::post('id_parent', 'int', '');
$about     = \xtetis\xengine\helpers\RequestHelper::post('about', 'str', '');

$model_date_group = new \xtetis\xforum\models\GroupModel(
    [
        'name'      => $name,
        'id'        => $id,
        'id_parent' => $id_parent,
        'about'     => $about,
    ]
);

if ($model_date_group->editGroup())
{

    $go_to_url = \xtetis\xcms\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'group',
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model_date_group->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

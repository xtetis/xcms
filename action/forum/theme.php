<?php

/**
 * Рендер списка тем
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);

$limit  = 20;
$offset = ($page - 1) * $limit;

$model_list = (new \xtetis\xforum\models\ThemeModel())->getThemesModelPageListInfo([
    'offset' => $offset,
    'order'  => 'id DESC',
]);

//print_r($model_theme_list); exit;

// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_start_list'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'theme',
    ],
]);

foreach ($model_list['models'] as $id => $model_theme)
{

    $urls['url_edit'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'edit_theme',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['url_undelete'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'undelete_theme',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['url_delete'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'delete_theme',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['url_unpublish'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'chpublish_theme',
        ],
        'query' => [
            'id'  => $id,
            'pub' => 0,
        ],
    ]);
    $urls['url_publish'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'chpublish_theme',
        ],
        'query' => [
            'id'  => $id,
            'pub' => 1,
        ],
    ]);
}
// ------------------------------------------------

$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $limit,
    $page,
    $model_list['count'],
    $urls['url_start_list']
);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_list' => $model_list,
        'urls'       => $urls,
        'pagination' => $pagination,
    ],
);

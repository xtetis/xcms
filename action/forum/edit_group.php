<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id','int',0);


$model_group = \xtetis\xforum\models\GroupModel::generateModelById($id);
if (!$model_group)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найдена группа');
}

// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_validate_edit_group'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_edit_group',
    ],
]);

$urls['url_group'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'group',
    ],
]);
// --------------------------------------------


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_group'    => $model_group,
        'urls'=>$urls,
    ],
);

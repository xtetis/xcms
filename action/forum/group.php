<?php

/**
 * Рендер списка групп
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$model_group_list = \xtetis\xforum\models\GroupModel::getAllGroupModels();

// Урлы
// ------------------------------------------------
$urls['url_cms_main']  = \xtetis\xcms\Component::makeUrl();
$urls['url_add_group'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'add_group',
    ],
]);
foreach ($model_group_list as $id => $model_group)
{
    $urls['url_edit_group'][$model_group->id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'edit_group',
        ],
        'query' => [
            'id' => $model_group->id,
        ],
    ]);
    $urls['url_undelete_group'][$model_group->id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'undelete_group',
        ],
        'query' => [
            'id' => $model_group->id,
        ],
    ]);
    $urls['url_delete_group'][$model_group->id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'delete_group',
        ],
        'query' => [
            'id' => $model_group->id,
        ],
    ]);
}
// ------------------------------------------------


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_group_list' => $model_group_list,
        'urls'                  => $urls,
    ],
);

<?php

/**
 * Рендер списка галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id_category = \xtetis\xengine\helpers\RequestHelper::get('id_category', 'int', 0);
$page        = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);

$limit  = 20;
$offset = ($page - 1) * $limit;

$where_arr = [];
if ($id_category)
{
    $where_arr = [
        'id_category = ' . $id_category,
    ];
}
$model_list_params = \xtetis\ximg\models\GalleryModel::getListModelsParams([
    'where'  => $where_arr,
    'limit'  => $limit,
    'offset' => $offset,
]);

// Урлы
// ------------------------------------------------
$urls['url_cms_main']    = \xtetis\xcms\Component::makeUrl();
$urls['url_add_gallery'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'add_gallery',
    ],
]);
$urls['url_validate_filter_gallery'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_filter_gallery',
    ],
]);

$urls['url_start_gallery_list'] = \xtetis\xcms\Component::makeUrl([
    'path'  => [
        \xtetis\xengine\App::getApp()->getAction(),
        'gallery',
    ],
    'query' => (($id_category) ? ['id_category' => $id_category] : []),
]);

foreach ($model_list_params['models'] as $id => $model)
{
    $urls['gallery_view'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'gallery_view',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['gallery_delete'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'gallery_delete',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);

}

// ------------------------------------------------

$pagination = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $limit,
    $page,
    $model_list_params['count'],
    $urls['url_start_gallery_list']
);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_list_params' => $model_list_params,
        'id_category'       => $id_category,
        'urls'              => $urls,
        'pagination'        => $pagination,
    ],
);

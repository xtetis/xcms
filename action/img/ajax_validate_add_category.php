<?php

/**
 * Валидация при добавлении темы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$name     = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');

$model = new \xtetis\ximg\models\CategoryModel(
    [
        'name'     => $name,
    ]
);

if ($model->addCategory())
{

    $go_to_url = \xtetis\xcms\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'category',
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

<?php

/**
 * Удаление категории галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


$id   = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$model = \xtetis\ximg\models\CategoryModel::generateModelById($id);

if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Категория не найдена');
}

$model->deleteCategory();


// --------------------------------------------
$urls['url_category'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'category',
    ],
]);
// --------------------------------------------

header("Location: ".$urls['url_category']);
exit;
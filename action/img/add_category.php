<?php

/**
 * Добавление категории галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');


// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_validate_add_category'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_add_category',
    ],
]);

$urls['url_category'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'category',
    ],
]);
// --------------------------------------------



// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'=>$urls,
    ],
);

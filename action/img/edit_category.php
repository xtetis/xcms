<?php

/**
 * Добавление категории галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model = \xtetis\ximg\models\CategoryModel::generateModelById($id);

if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Категория не найдена');
}

// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_validate_add_category'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_edit_category',
    ],
]);

$urls['url_category'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'category',
    ],
]);
// --------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'  => $urls,
        'model' => $model,
    ],
);

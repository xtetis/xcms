<?php

/**
 * Валидация при добавлении группы
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$id_category = \xtetis\xengine\helpers\RequestHelper::post('id_category', 'int', '');

$go_to_url = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'gallery',
    ],
    'query'=>[
        'id_category'=>$id_category
    ]
]);

$response['result']            = true;
$response['data']['go_to_url'] = $go_to_url;


echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

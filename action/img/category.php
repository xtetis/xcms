<?php

/**
 * Рендер списка групп
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$model_category_list = \xtetis\ximg\models\CategoryModel::getAllCategoryModels();

// Урлы
// ------------------------------------------------
$urls['url_cms_main']     = \xtetis\xcms\Component::makeUrl();
$urls['url_add_category'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'add_category',
    ],
]);
foreach ($model_category_list as $id => $model_category)
{
    $urls['url_edit_category'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'edit_category',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
    $urls['url_delete_category'][$id] = \xtetis\xcms\Component::makeUrl([
        'path'  => [
            \xtetis\xengine\App::getApp()->getAction(),
            'delete_category',
        ],
        'query' => [
            'id' => $id,
        ],
    ]);
}
// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_category_list' => $model_category_list,
        'urls'                => $urls,
    ],
);

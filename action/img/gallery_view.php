<?php

/**
 * Рендер просмотра галереи
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model = \xtetis\ximg\models\GalleryModel::generateModelById($id);

if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Галерея не найдена');
}

// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
$urls['url_gallery']  = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'gallery',
    ],
]);
// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model' => $model,
        'urls'  => $urls,
    ],
);

<?php
/**
 * Рендер главной страницы CMS
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$model_migrarion = new \xtetis\xcms\models\MigrationModel;

$model_migrarion->getComponentListToMigrate();
if ($model_migrarion->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_migrarion->getLastErrorMessage());
}

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_migrarion' => $model_migrarion,
    ],
);

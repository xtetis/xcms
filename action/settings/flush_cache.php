<?php

/**
 * Рендер списка групп
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$by_tag = \xtetis\xengine\helpers\RequestHelper::get('by_tag', 'str', '');

// Урлы
// ------------------------------------------------
$urls['url_cms_main']  = \xtetis\xcms\Component::makeUrl();
$urls['url_cache'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'cache',
    ],
]);

// ------------------------------------------------
$cache = \xtetis\xengine\libraries\Cache::getCacheObj();
if (strlen($by_tag))
{
    $cache->deleteByTag($by_tag);
}
else
{
    $count_files_cache = $cache->flush();
}



header("Location: ".$urls['url_cache']);
exit;

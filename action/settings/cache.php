<?php

/**
 * Рендер списка групп
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'manager');

$model_group_list = \xtetis\xforum\models\GroupModel::getAllGroupModels();

// Урлы
// ------------------------------------------------
$urls['url_cms_main']    = \xtetis\xcms\Component::makeUrl();
$urls['url_flush_cache'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'flush_cache',
    ],
]);

// ------------------------------------------------
$cache             = \xtetis\xengine\libraries\Cache::getCacheObj();
$count_files_cache = $cache->getTotalCount();
$cache_tags        = $cache->getTagList();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'              => $urls,
        'count_files_cache' => $count_files_cache,
        'cache_tags'        => $cache_tags,
        'cache'             => $cache,
    ],
);

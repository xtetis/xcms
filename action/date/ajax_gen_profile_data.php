<?php

/**
 * Генерация случайных данных пользователя
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id_profile_type = rand(1, 2);
if (1 == $id_profile_type)
{
    $filename_names = __DIR__ . '/../../data/bot_names/ru/male_names.txt';
}
else
{
    $filename_names = __DIR__ . '/../../data/bot_names/ru/female_names.txt';
}

if (!file_exists($filename_names))
{
    die('Не найден файл с именами');
}

$names      = file_get_contents($filename_names);
$names      = explode("\n", $names);
$rand_index = array_rand($names, 1);
$name       = $names[$rand_index];

function getAllDates(
    $startingDate,
    $endingDate
)
{
    $datesArray = [];

    $startingDate = strtotime($startingDate);
    $endingDate   = strtotime($endingDate);

    for ($currentDate = $startingDate; $currentDate <= $endingDate; $currentDate += (86400))
    {
        $date         = date('d.m.Y', $currentDate);
        $datesArray[] = $date;
    }

    return $datesArray;
}

$dates      = getAllDates('1980-01-01', '2000-01-01');
$rand_index = array_rand($dates, 1);
$date       = $dates[$rand_index];

$response['js_success']              = 'cms_gen_anket.prepareGenerateUserData();';
$response['result']                  = true;
$response['data']['id_profile_type'] = $id_profile_type;
$response['data']['name']            = $name;
$response['data']['birth_date']      = $date;
$response['data']['about']           = '';
$response['data']['height']          = rand(155, 189);
$response['data']['weight']          = rand(55, 95);
$response['data']['orientation']     = rand(1, 2);

$sql = '
SELECT id FROM `xgeo_object` WHERE `id` NOT IN
(
SELECT `id`
FROM `xgeo_object` t
WHERE
`id` IN
	(
		SELECT * FROM (SELECT DISTINCT `id_parent` id FROM `xgeo_object`) zz
	)
)
AND
	`id_geo_type` = 3
AND
 `id` NOT IN (
 SELECT DISTINCT `id_geo_object` FROM `xdate_profile`
 )
ORDER BY RAND()
LIMIT 1
';

$model_list = \xtetis\xgeo\models\ObjectModel::getModelListBySql($sql);
if (!$model_list)
{
    die('Не найден ГЕО объект');
}

foreach ($model_list as $id => $model)
{
    $response['data']['id_geo_object']                                       = $model->id;
    $response['text_value']['id_geo_object_select_multilevel_selected_text'] = $model->getFullTreePath();

}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

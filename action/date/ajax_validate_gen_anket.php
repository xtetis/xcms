<?php

/**
 * Генерация случайных данных пользователя
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

$response['js_success'] = 'alert("OK");';
$response['js_fail']    = '';

$model = new \xtetis\xcms\models\GenDateAnketModel();

// Загружает данные из POST параметров
$model->loadPostData();

if ($model->run())
{
    $response['result']            = true;
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;


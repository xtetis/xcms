<?php

/**
 * Список галерей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

// --------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();

$urls['url_validate'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_gen_anket',
    ],
]);

$urls['url_gen_profile_data'] = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_gen_profile_data',
    ],
]);

$urls['xgeo_multiple_data_url'] = \xtetis\xgeo\Component::makeUrl([
    'path' => [
        'data',
        'ajax_js_tree',
    ],
]);

// --------------------------------------------


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'       => $urls,
    ],
);

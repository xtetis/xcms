<?php

namespace xtetis\xcms\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class MenuModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xcms_menu';

    /**
     * Родительский пункт меню
     */
    public $id_parent = 0;

    /**
     * Имя пункта меню
     */
    public $name = '';

    /**
     * Содержимое пункта меню
     */
    public $content = '';

    /**
     * Тип ссылки
     */
    public $type = 0;

    /**
     * Дата создания записи
     */
    public $create_date = '';

    /**
     * Добавляет пункт меню
     */
    public function addMenu()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name = strval($this->name);
        $this->name = strip_tags($this->name);

        $this->content = strval($this->content);
        $this->content = strip_tags($this->content);

        $this->type = intval($this->type);

        $this->id_parent = intval($this->id_parent);

        if (!in_array($this->type, [0, 1]))
        {
            $this->type = 0;
        }

        if (!$this->id_parent)
        {
            $this->id_parent = null;
        }
        else
        {
            $model_parent = self::generateModelById($this->id_parent);
            if (!$model_parent)
            {
                $this->addError('id_parent', 'Указан некорректный родительский пункт меню');

                return false;
            }

            if (intval($model_parent->id_parent))
            {
                $this->addError('id_parent', 'Родительский пункт меню может быть только такой, у которого родитель - корень');

                return false;
            }
        }

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано имя');

            return false;
        }

        $this->insert_update_field_list = [
            'name',
            'content',
            'type',
            'id_parent',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Редактирует пункт меню
     */
    public function editMenu()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $this->name = strval($this->name);
        $this->name = strip_tags($this->name);

        $this->content = strval($this->content);
        $this->content = strip_tags($this->content);

        $this->type = intval($this->type);

        $this->id_parent = intval($this->id_parent);

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Пункт меню не существует');

            return false;
        }

        if (!in_array($this->type, [0, 1]))
        {
            $this->type = 0;
        }

        if (!$this->id_parent)
        {
            $this->id_parent = null;
        }
        else
        {
            $model_parent = self::generateModelById($this->id_parent);
            if (!$model_parent)
            {
                $this->addError('id_parent', 'Указан некорректный родительский пункт меню');

                return false;
            }

            if (intval($model_parent->id_parent))
            {
                $this->addError('id_parent', 'Родительский пункт меню может быть только такой, у которого родитель - корень');

                return false;
            }
        }

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано имя');

            return false;
        }

        if ($this->type == 1)
        {
            $filename = ENGINE_DIRECTORY.$this->content;
            if (!file_exists($filename))
            {
                $this->addError('content', 'Файл не существует');

                return false;
            }

            $file_content = file_get_contents($filename);

            if (strpos($file_content, "(!defined('SYSTEM'))") === false)
            {
                $this->addError('content', 'В исполняемом файле не найдено определение SYSTEM');

                return false;
            }
        }

        $this->insert_update_field_list = [
            'name',
            'content',
            'type',
            'id_parent',
        ];

        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает список тем для выбора в качестве родительской
     * (при редактировании списка тем а вдминке) - возвращает только корень или те,
     * у кого родительская тема - корневая
     */
    public function getOptionsParent($add_empty = true)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = 'SELECT id FROM xcms_menu WHERE COALESCE(id_parent,0) = 0';

        $models = self::getModelListBySql(
            $sql
        );

        $options = [];

        if ($add_empty)
        {
            $options[0] = 'Корень';
        }

        foreach ($models as $id => $model)
        {
            $prefix = '';
            if ($add_empty)
            {
                $prefix = ' → ';
            }

            $options[$id] = $prefix . $model->name;
        }

        return $options;
    }
}

<?php

namespace xtetis\xcms\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class GenDateAnketModel extends \xtetis\xengine\models\Model
{

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var int
     */
    public $id_profile_type = 0;

    /**
     * @var int
     */
    public $id_geo_object = 0;

    /**
     * @var string
     */
    public $birth_date = '';

    /**
     * @var string
     */
    public $about = '';

    /**
     * @var int
     */
    public $height = 0;

    /**
     * @var int
     */
    public $weight = 0;

    /**
     * @var int
     */
    public $orientation = 0;

    /**
     * @var array
     */
    public $image = [];

    /**
     * Урл с изображением
     */
    public $url = '';

    /**
     * BASE64 файла
     */
    public $file_base64 = '';

    /**
     * BASE64 файла для возврата
     */
    public $file_base64_return = '';

    /**
     * Данные файла
     */
    public $file_data = false;

    /**
     * MIME тип файла
     */
    public $mime_content_type = '';

    /**
     * Допустимые MIME типы файла
     */
    public $allow_mime_type_list = [
        'image/jpeg'          => 'jpg',
        'image/x-citrix-jpeg' => 'jpg',
        'image/pjpeg'         => 'jpg',
        'image/png'           => 'png',
    ];

    /**
     * Максимальный размер файла в байтах 512kb
     */
    public $max_filesize = 2 * 1024 * 1024;

    /**
     * Размер файла
     */
    public $filesize = 0;

    /**
     * Обрабатывает данные
     */
    public function run()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->name            = strval($this->name);
        $this->id_profile_type = intval($this->id_profile_type);
        $this->id_geo_object   = intval($this->id_geo_object);
        $this->birth_date      = strval($this->birth_date);
        $this->about           = strval($this->about);
        $this->height          = intval($this->height);
        $this->weight          = intval($this->weight);
        $this->orientation     = intval($this->orientation);

        if (!$this->id_profile_type)
        {
            $this->addError('id_profile_type', 'Не указан тип профиля');

            return false;
        }

        if (!mb_strlen($this->about))
        {
            $this->addError('about', 'Напишите о себе несколько слов');

            return false;
        }

        if (!isset((new \xtetis\xdate\models\ProfileModel())->getHeightOptions(true)[$this->height]))
        {
            $this->addError('height', 'Укажите корректный возраст');

            return false;
        }

        if (!isset((new \xtetis\xdate\models\ProfileModel())->getWeightOptions(true)[$this->weight]))
        {
            $this->addError('weight', 'Укажите корректный вес');

            return false;
        }

        if (!strlen($this->birth_date))
        {
            $this->addError('birth_date', 'Не указана дата рождения');

            return false;
        }

        if (!preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $this->birth_date))
        {
            $this->addError('birth_date', 'Некорректный формат даты рождения');

            return false;
        }

        list($d, $m, $y) = array_pad(explode('.', $this->birth_date, 3), 3, 0);

        if (!ctype_digit("$d$m$y"))
        {
            $this->addError('birth_date', 'Некорректная дата рождения');

            return false;
        }

        if (!checkdate($m, $d, $y))
        {
            $this->addError('birth_date', 'Некорректная дата рождения');

            return false;
        }

        $birth_date = strtotime($this->birth_date);

        if ($birth_date > strtotime('-18 years', time()))
        {
            $this->addError('birth_date', 'Только для тех, кто достиг 18 лет');

            return false;
        }

        if ($birth_date < strtotime('-99 years', time()))
        {
            $this->addError('birth_date', 'Вам слишком много лет');

            return false;
        }

        $date_profile_type_model   = new \xtetis\xdate\models\ProfileTypeModel();
        $date_profile_type_options = $date_profile_type_model->getOptions();

        if (!isset($date_profile_type_options[$this->id_profile_type]))
        {
            $this->addError('id_profile_type', 'Не указан тип профиля');

            return false;
        }

        if (!$this->id_geo_object)
        {
            $this->addError('id_geo_object', 'Не указан населенный пункт');

            return false;
        }

        $model_geo_object = \xtetis\xgeo\models\ObjectModel::generateModelById($this->id_geo_object);
        if (!$model_geo_object)
        {
            $this->addError('id_geo_object', 'Населенный пункт не существует');

            return false;
        }

        //print_r($this);

        $model_user_bot = new \xtetis\xuser\models\UserBotModel(
            [
                'name' => $this->name,
            ]
        );
        if (!$model_user_bot->generateBot())
        {
            $this->addError('name', $model_user_bot->getLastErrorMessage());

            return false;
        }

        //print_r($model_user_bot);

        $model_profile = new \xtetis\xdate\models\ProfileModel([
            'id_user'         => intval($model_user_bot->id_user),
            'birth_date'      => strval($this->birth_date),
            'id_profile_type' => intval($this->id_profile_type),
            'id_geo_object'   => intval($this->id_geo_object),
            'about'           => strval($this->about),
            'height'          => intval($this->height),
            'weight'          => intval($this->weight),
        ]);
        if (!$model_profile->saveDateProfile())
        {
            $this->addError('common', $model_profile->getLastErrorMessage());

            return false;
        }

        //print_r($model_profile);

        $model_date_album = new \xtetis\xdate\models\DateAlbumModel([
            'name'    => 'ddd',
            'id_user' => $model_user_bot->id_user,

        ]);

        if (!$model_date_album->addDateAlbum())
        {
            $this->addError('common', $model_date_album->getLastErrorMessage());

            return false;
        }

        //print_r($model_date_album);

        //$this->id_gallery

        foreach ($this->image as $image_list)
        {
            foreach ($image_list as $key => $image)
            {
                $model_img_upload = new \xtetis\ximg\models\ImgUploadModel([
                    'id_gallery' => $model_date_album->id_gallery,
                    'type'       => 'base64',
                    'filedata'   => $image,
                ]);

                if (!$model_img_upload->uploadFile())
                {
                    $this->addError('common', $model_img_upload->getLastErrorMessage());

                    return false;
                }

                //print_r($model_img_upload);


                $img_model = new \xtetis\ximg\models\ImgModel([
                    'id_gallery' => $model_date_album->id_gallery,
                    'filename'   => $model_img_upload->upload_full_path,
                ]);
                if (!$img_model->addImgFromFile())
                {
                
                    unlink($model_img_upload->upload_full_path);

                    $this->addError('common', $img_model->getLastErrorMessage());

                    return false;
                }
            }
        }

        return true;
    }

}

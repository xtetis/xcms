<?php

namespace xtetis\xcms\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class MigrationModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xcms_migration';

    /**
     * ID
     */
    public $id = '';

    /**
     * Классы компонентов для миграции
     */
    public $component_classes_to_migrate = [];

    /**
     * Текущий класс компонента
     */
    public $current_component_class = '';

    /**
     * ФАйлы миграций для компонентов
     */
    public $files_to_migrate = [];

    /**
     * @return mixed
     */
    public function getComponentListToMigrate()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $component_url_list = \xtetis\xengine\Config::getComponentUrlList();

        $this->component_classes_to_migrate = [];

        foreach ($component_url_list as $component_url => $component_class)
        {
            $this->current_component_class = $component_class;
            
            if (!$this->getComponentMigrateFiles())
            {
                continue;
            }

            $this->component_classes_to_migrate[] = $component_class;
        }

        return $this->component_classes_to_migrate;
    }

    /**
     * Полуучает список файлов миграций для компонентов
     */
    public function getComponentMigrateFiles()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->current_component_class = strval($this->current_component_class);

        if (!strlen($this->current_component_class))
        {
            $this->addError('current_component_class', __FUNCTION__ . ': Не указан параметр current_component_class');

            return false;
        }

        $component_class                          = $this->current_component_class;
        $this->files_to_migrate[$component_class] = [];

        $dir           = $component_class::getComponentDirectory();
        $dir_migration = $dir . '/migration';
        if (!file_exists($dir_migration))
        {
            return [];
        }

        if (!is_dir($dir_migration))
        {
            return [];
        }

        if (!is_readable($dir_migration))
        {
            return [];
        }

        $search_dir = $dir_migration . '/*.php';
        $list       = glob($search_dir);
        natsort($list);
        foreach (array_reverse($list) as $filename)
        {
            $this->files_to_migrate[$component_class][] = $filename;
        }

        return $this->files_to_migrate[$component_class];
    }


    public function runMigrate()
    {
        
    }
}

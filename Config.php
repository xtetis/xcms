<?php

namespace xtetis\xcms;

class Config extends \xtetis\xengine\models\Model
{
    /**
     * Проверяет параметры
     */
    public static function validateParams()
    {
        if (!defined('MANAGER_RUN_COMPONENT_ACTION'))
        {
            // Часть урла, которая обрабатывает админскую часть компонента
            define('MANAGER_RUN_COMPONENT_ACTION', 'component_admin');
        }
    }

}

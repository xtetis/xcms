<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Знакомства - Генерация анкет',
        ],
    ]);

    // Добавляем файл JS для обработки страницы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/cms_gen_anket.js');

?>



<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_gen_profile_data'],
    'form_type'    => 'ajax',
    'form_id'=> 'gen_profile_data'
]);?>
<?=\xtetis\xform\Component::renderFormEnd();?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Генерация анкеты и бота</h4>

<button type="button" class="btn btn-primary" id="gen_profile_data">Генерировать профиль</button>
<button type="button" class="btn btn-primary" id="gen_photo">Генерировать фото</button>
<br>
<br>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Тип анкеты',
            'name'    => 'id_profile_type',
            'options' => (new \xtetis\xdate\models\ProfileTypeModel())->getOptions(),

        ],
        'value'      => 0,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя',
            'name'  => 'name',
        ],
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select_multilevel',
        'attributes' => [
            'label'         => 'Населенный пункт',
            'name'          => 'id_geo_object',
            'selected_text' => '', //$model_date_profile->getProfileLocationFullText(),
            'placeholder' => 'Выберите населенный пункт',
            'data_url'      => $urls['xgeo_multiple_data_url'],
        ],
        'value'      => 0,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label'    => 'Дата рождения',
            'name'     => 'birth_date',
            'class'    => 'date-control form-control',
            'readonly' => 'readonly',
        ],
        'value'      => '',
    ]
)?>



<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Обо мне (в анкете)',
            'name'  => 'about',
            'style' => 'min-height: 150px;height: 110px;',

        ],
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Рост (см)',
            'name'    => 'height',
            'options' => \xtetis\xdate\models\ProfileModel::getHeightOptions(true),
        ],
        'value'      => '',
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Вес (кг)',
            'name'    => 'weight',
            'options' => \xtetis\xdate\models\ProfileModel::getWeightOptions(true),
        ],
        'value'      => '',
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Ориентация',
            'name'    => 'orientation',
            'options' => \xtetis\xdate\models\ProfileModel::getOrientationOptions(true),
        ],
        'value'      => '',
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_image_noaulbum',
        'attributes' => [
            'label'             => 'Изображение',
            'name'              => 'image[]',
            'max_images'        => 1,
            'external_img_data' => 1,
        ],
    ]
)?>
<button type="submit"
        class="btn btn-block btn-primary mb-4">Добавить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


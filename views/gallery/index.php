<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


\xtetis\xengine\App::getApp()->setParam('breadcrumbs',[
    [
        'url'=> $url_cms_main,
        'name'=>'Админка',
    ],
    [
        'name'=>'Галереи',
    ],
]);

// Добавляем файл JS для обработки формы
\xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/cms_ximg.js');
$id_gallery = \xtetis\xengine\helpers\RequestHelper::get('id_gallery', 'int', 0);
$id_gallery_assign = $id_gallery;
$current_gallery_model = false;
if (!$id_gallery)
{
    $id_gallery_assign = -1;
}
else
{
    $current_gallery_model = new \xtetis\ximg\models\GalleryModel([
        'id'=>$id_gallery
    ]);

    $current_gallery_model->getById();
}

?>
<input type="hidden"
       id="url__cms_galleries_list"
       value="<?=$url_cms_gallery?>" />
<hr>
<div id="jstree_gallery">
    <?=$model_root_gallery->getGalleryListHtml(0,true) ?>
</div>


<div class="gallery_container">
    <div class="gallery_container_inner">
        <div style="<?=($id_gallery)?'':'display:none;'?>">
            <hr>
            <?php 
            /*
echo \xtetis\xanyfield\Component::renderForm([
        'component'          => 'gallery',
        'component_category' => 'edit',
        'value_assign_key'   => $id_gallery_assign,
]);
*/
?>
            <hr>
            <h4>Изображения</h4>

            <?php if ($current_gallery_model):?>
            <div class="p-3"
                 style="background: gray;">
                <div class="card-columns">
                    <?php foreach ($current_gallery_model->getImgModelList() as $id_img => $model_img) : ?>
                    <div class="card">
                        <img class="img-fluid card-img-top"
                             src="<?=$model_img->getImgSrc()?>"
                             alt="Card image cap">
                        <div class="card-body">
                            <button class="btn  btn-danger">Delete</button>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endif; ?>







            <div class="">
                <br>
                <div class="text-center">
                    <?=\xtetis\xform\Component::renderOnlyFormStart([
                    'url_validate' => '/',
                    'form_type'    => 'ajax',
                    ]);?>
                    <?=\xtetis\xform\Component::renderField(['template'=>'input_image'])?>
                    <?=\xtetis\xform\Component::renderFormEnd();?>
                </div>


                <div class="input-group cust-file-button">
                    <div class="custom-file">
                        <input type="file"
                               accept="image/png, image/jpeg"
                               class="custom-file-input"
                               id="inputGroupFile04">
                        <label class="custom-file-label"
                               for="inputGroupFile04">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <button class="btn  btn-primary"
                                type="button">Button</button>
                    </div>
                </div>
                <div id="logoPreview">
                    <img src=""
                         id="img_prev"
                         alt="">
                </div>
            </div>
        </div>
    </div>
</div>

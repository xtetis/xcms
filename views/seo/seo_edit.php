<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_seo'],
            'name' => 'SEO',
        ],
        [
            'name' => 'Добавить SEO',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">
    <?php if ($model->id):?>
        Редактирование
    <?php else: ?>
        Создание
    <?php endif; ?> 
        SEO
</h4>

<input type="hidden" name="id" value="<?=$model->id?>">

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Урл',
            'name'  => 'url',
        ],
        'value'=>$model->url
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя страницы',
            'name'  => 'name',
        ],
        'value'=>$model->name
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Title страницы',
            'name'  => 'title',
        ],
        'value'=>$model->title
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'H1 страницы',
            'name'  => 'hone',
        ],
        'value'=>$model->hone
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Description',
            'name'  => 'description',
            'style' => 'min-height: 150px;height: 110px;',

        ],
        'value'      => $model->description,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Сеотекст',
            'name'  => 'seotext',
            'class' => 'tinymce form-control',

        ],
        'value'      => $model->seotext,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Использовать SEF параметры',
            'name'    => 'use_sef_params',
            'options' => [
                0=>'Нет',
                1=>'Да',
            ],
        ],
        'value'      =>$model->use_sef_params,
    ])
?>



<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Route component',
            'name'  => 'sef_route_component',
        ],
        'value'=>$model->sef_route_component
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Route action',
            'name'  => 'sef_route_action',
        ],
        'value'=>$model->sef_route_action
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Route query',
            'name'  => 'sef_route_query',
        ],
        'value'=>$model->sef_route_query
    ]
)?>

<?=\xtetis\xform\Component::renderField(
        [
            'template'   => 'input_text',
            'attributes' => [
                'label' => 'Route GET параметры',
                'name'  => 'sef_route_get',
            ],
            'value'=>$model->sef_route_get
        ]
    )?>




<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Использовать редирект',
            'name'    => 'use_redirect',
            'options' => [
                0=>'Нет',
                1=>'Да',
            ],
        ],
        'value'      =>$model->use_redirect,
    ])
?>




<?=\xtetis\xform\Component::renderField(
        [
            'template'   => 'input_text',
            'attributes' => [
                'label' => 'На какой урл редиректить',
                'name'  => 'redirect_to',
            ],
            'value'=>$model->redirect_to
        ]
    )?>


<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


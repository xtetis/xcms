<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Сео',
        ],
    ]);



?>

<div class="btn-group" role="group" aria-label="Basic example">


<a href="<?=$urls['url_add_seo']?>"
   class="btn  btn-primary">
    Добавить страницу
</a>
<a href="<?=$urls['url_add_seo_link']?>"
   class="btn  btn-primary">
    Добавить ссылку
</a>
</div>


<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Урл</th>
                    <th>Имя</th>
                    <th>Active</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_list['models'] as $id => $model): ?>
                <?php
                $tr_class = '';
                if (!$model->active)
                {
                    $tr_class = 'bg-danger';
                }
                ?>
                <tr  class=" bg-info">
                    <td>
                        <a href="<?=$model->url?>" target="_blank" rel="noopener noreferrer">
                        <?=$model->id?>
                        </a>
                    </td>
                    <td><?=$model->url?></td>
                    <td>
                        <?=$model->name?>
                        <br>
                        use_redirect = <?=$model->use_redirect?>
                        <br>
                        use_sef_params = <?=$model->use_sef_params?>
                    </td>
                    <td class="<?=$tr_class?>"><?=intval($model->active)?></td>
                    <td>

                        <a href="<?=$urls['url_edit'][$id]?>">
                            <i class="far fa-edit"></i>
                        </a>

                        <?php if ($model->active): ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Деактивировать ?');"
                           idx="<?=$id?>"
                           href="<?=$urls['url_unactivate'][$id]?>">
                           <i class="fas fa-eye-slash"></i>
                        </a>
                        <?php else: ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Активировать ?');"
                           idx="<?=$id?>"
                           href="<?=$urls['url_activate'][$id]?>">
                           <i class="fas fa-eye"></i>
                        </a>
                        <?php endif;?>

                        <a class="cms_article_set_deleted"
                           onclick="return confirm('Удалить ?');"
                           href="<?=$urls['url_delete'][$id]?>">
                            <i class="fas fa-trash"></i>
                        </a>

                    </td>
                </tr>
                <?php foreach ($model->getSeoLinkModels() as $model_seo_link): ?>
                <tr class="bg-success">
                    <td>
                        <?=$model_seo_link->id?>
                    </td>
                    <td colspan="3">
                        <?=$model_seo_link->anchor?>
                    </td>
  
                    <td>
                        <a href="<?=$urls['url_seo_link_edit'][$model_seo_link->id]?>">
                            <i class="far fa-edit"></i>
                        </a>
                        <a class="cms_article_set_deleted"
                           onclick="return confirm('Удалить ?');"
                           href="<?=$urls['url_seo_link_delete'][$model_seo_link->id]?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

<div class="">
    <?=$pagination?>
</div>
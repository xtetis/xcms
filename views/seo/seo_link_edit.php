<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_seo'],
            'name' => 'SEO',
        ],
        [
            'name' => 'Добавить SEO Link',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">
    <?php if ($model->id):?>
        Редактирование
    <?php else: ?>
        Создание
    <?php endif; ?> 
        SEO Link
</h4>

<input type="hidden" name="id" value="<?=$model->id?>">

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Анкор',
            'name'  => 'anchor',
        ],
        'value'=>$model->anchor
    ]
)?>
<div>
    Также может быть текстом. Для вставки ссылки в 
    конкретное место необходимо использовать
    знак "#", например "текст текст #анкор# текст текст"
</div>
<br>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Страница',
            'name'    => 'id_seo_page',
            'options' => (new \xtetis\xseo\models\SeoPageModel())->getOptionsParent(),

        ],
        'value'      => $model->id_seo_page,
    ]
)?>

<br>

<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


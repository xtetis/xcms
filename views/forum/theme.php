<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Форум - Темы',
        ],
    ]);



?>







<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Published</th>
                    <th>Del</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_list['models'] as $id => $model_theme): ?>
                <?php
                $tr_class = '';
                if ($model_theme->deleted)
                {
                    $tr_class = 'table-danger';
                }
                elseif(!intval($model_theme->published))
                {
                    $tr_class = 'table-success';
                }
                ?>
                <tr  class="<?=$tr_class?>">
                    <td>
                        <a href="<?=$model_theme->getLink()?>" target="_blank" rel="noopener noreferrer">
                        <?=$model_theme->id?>
                        </a>
                    </td>
                    <td style="white-space: normal;">
                    <?=$model_theme->name?>
                    </td>
                    <td><?=$model_theme->create_date?></td>
                    <td><?=intval($model_theme->published)?></td>
                    <td><?=intval($model_theme->deleted)?></td>
                    <td>

                        <a href="<?=$urls['url_edit'][$id]?>">
                            <i class="far fa-edit"></i>
                        </a>

                        <?php if ($model_theme->published): ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Снять с публикации ?');"
                           idx="<?=$id?>"
                           href="<?=$urls['url_unpublish'][$id]?>">
                           <i class="fas fa-eye-slash"></i>
                        </a>
                        <?php else: ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Опубликовать ?');"
                           idx="<?=$id?>"
                           href="<?=$urls['url_publish'][$id]?>">
                           <i class="fas fa-eye"></i>
                        </a>
                        <?php endif;?>


                        <?php if ($model_theme->deleted): ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Вернуть из корзины ?');"
                           idx="<?=$id?>"
                           href="<?=$urls['url_undelete'][$id]?>">
                            <i class="fas fa-trash-restore"></i>
                        </a>
                        <?php endif;?>

                        <a class="cms_article_set_deleted"
                           idx="<?=$id?>"
                           onclick="return confirm('Удалить ?');"
                           deleted="<?=$model_theme->deleted?>"
                           href="<?=$urls['url_delete'][$id]?>">
                            <i class="fas fa-trash"></i>
                        </a>

                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

<div class="">
    <?=$pagination?>
</div>
<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_group'],
            'name' => 'Группы',
        ],
        [
            'name' => 'Добавить группу',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_add_group'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Добавление группы</h4>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Родительская тема',
            'name'    => 'id_parent',
            'options' => (new \xtetis\xforum\models\GroupModel())->getOptionsParent(),

        ],
        'value'      => 0,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя группы',
            'name'  => 'name',
        ],
    ]
)?>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Описание',
            'name'  => 'about',
            'style' => 'min-height: 150px;height: 110px;',

        ],
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_image_noaulbum',
        'attributes' => [
            'label'      => 'Изображение',
            'name'       => 'image[]',
            'max_images' => 1,
        ],
    ]
)?>
<button type="submit"
        class="btn btn-block btn-primary mb-4">Добавить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_theme'],
            'name' => 'Темы',
        ],
        [
            'name' => 'Редактировать тему',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_edit_theme'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Редактирование темы</h4>
<input type="hidden" name="id" value="<?=$model->id?>">
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Родительская группа',
            'name'    => 'id_group',
            'options' => (new \xtetis\xforum\models\GroupModel())->getOptionsAddTheme(),

        ],
        'value'      => $model->id_group,
    ]
)?>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Название темы',
            'name'  => 'name',
        ],
        'value'      => $model->name,
    ]
)?>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Текст',
            'name'  => 'about',
            'style' => 'min-height: 150px;height: 110px;',

        ],
        'value'      => $model->about,
    ]
)?>

<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


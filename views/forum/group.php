<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Знакомства - Группы',
        ],
    ]);



?>

<a href="<?=$urls['url_add_group']?>"
   class="btn  btn-primary"
   style="float: right; margin-bottom: 10px;">
    Добавить группу
</a>





<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Del</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_group_list as $id => $model_group): ?>
                <?php if (!intval($model_group->id_parent)): ?>
                <tr <?=($model_group->deleted) ? ' class="table-danger"' : ''?>>
                    <td><?=$model_group->id?></td>
                    <td><?=$model_group->name?></td>
                    <td><?=$model_group->create_date?></td>
                    <td><?=$model_group->deleted?></td>
                    <td>
                        <a href="<?=$urls['url_edit_group'][$model_group->id]?>">
                            <i class="far fa-edit"></i>
                        </a>


                        <?php if ($model_group->deleted): ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Вернуть из корзины ?');"
                           idx="<?=$model_group->id?>"
                           href="<?=$urls['url_undelete_group'][$model_group->id]?>">
                            <i class="fas fa-trash-restore"></i>
                        </a>
                        <?php endif;?>

                        <a class="cms_article_set_deleted"
                           idx="<?=$model_group->id?>"
                           onclick="return confirm('Удалить ?');"
                           deleted="<?=$model_group->deleted?>"
                           href="<?=$urls['url_delete_group'][$model_group->id]?>">
                            <i class="fas fa-trash"></i>
                        </a>

                    </td>
                </tr>
                <?php foreach ($model_group_list as $id => $model_group_child): ?>
                <?php if (intval($model_group_child->id_parent) == $model_group->id): ?>
                <tr <?=($model_group_child->deleted) ? ' class="table-danger"' : ''?>>
                    <td><?=$model_group_child->id?></td>
                    <td>
                        <span>
                            &#8627;
                        </span>
                        <?=$model_group_child->name?>
                    </td>
                    <td><?=$model_group_child->create_date?></td>
                    <td><?=$model_group_child->deleted?></td>
                    <td>
                        <a href="<?=$urls['url_edit_group'][$model_group_child->id]?>">
                            <i class="far fa-edit"></i>
                        </a>


                        <?php if ($model_group_child->deleted): ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Вернуть из корзины ?');"
                           idx="<?=$model_group_child->id?>"
                           href="<?=$urls['url_undelete_group'][$model_group_child->id]?>">
                            <i class="fas fa-trash-restore"></i>
                        </a>
                        <?php endif;?>

                        <a class="cms_article_set_deleted"
                           idx="<?=$model_group_child->id?>"
                           onclick="return confirm('Удалить ?');"
                           deleted="<?=$model_group_child->deleted?>"
                           href="<?=$urls['url_delete_group'][$model_group_child->id]?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
                <?php endif;?>
                <?php endforeach;?>
                <?php endif;?>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

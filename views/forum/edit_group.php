<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_group'],
            'name' => 'Группы',
        ],
        [
            'name' => 'Редактировать группу',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_edit_group'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Редактирование группы</h4>
<input type="hidden" name="id" value="<?=$model_group->id?>">
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Родительская тема',
            'name'    => 'id_parent',
            'options' => (new \xtetis\xforum\models\GroupModel())->getOptionsParent(),

        ],
        'value'      => $model_group->id_parent,
    ]
)?>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя группы',
            'name'  => 'name',
        ],
        'value'      => $model_group->name,
    ]
)?>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Описание',
            'name'  => 'about',
            'style' => 'min-height: 150px;height: 110px;',

        ],
        'value'      => $model_group->about,
    ]
)?>

<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


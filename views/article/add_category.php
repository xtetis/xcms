<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_cms_main,
            'name' => 'Админка',
        ],
        [
            'url'  => $url_category_list,
            'name' => 'Категории статей',
        ],
        [
            'name' => 'Добавить категорию',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_validate_add_category,
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Добавление категории</h4>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя категории',
            'name'  => 'name',
        ],
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Родительская категория',
            'name'    => 'id_parent',
            'options' => $model_article_category->getOptions(),
        ],
    ]
)?>
<button type="submit"
        class="btn btn-block btn-primary mb-4">Добавить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


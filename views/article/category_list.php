<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  =>  $url_cms_main ,
            'name' => 'Админка',
        ],
        [
            'name' => 'Категории статей',
        ],
    ]);

    // Добавляем файл JS для обработки страницы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/cms_article_category.js');




?>






<div id="jstree_article_category">
    <?=$model_root_article_category->getListHtml(0, true)?>
</div>
<div class="p-3 text-center mod_cms_article_category_info_block">
    <div class="article_category_name"></div>
    <br>
    <a href="<?=$url_add_category?>"
       class="btn btn_article_category_add_link btn-primary">Добавить</a>
    <span>
        <a href=""
           href_start="<?=$url_edit_category_start?>"
           class="btn btn_article_category_edit_link btn-primary">Редактировать</a>
        <a href=""
           href_start="<?=$url_delete_category_start?>"
           onclick="return confirm('Удалить категорию статей?');"
           class="btn btn_article_category_delete_link btn-primary">Удалить</a>
    </span>
</div>

<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/cms_ximg.js');

    


    $page_name = 'Добавить статью';
    if ($model_article->id)
    {
        $page_name = 'Редактировать статью';
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_article_list'],
            'name' => 'Статьи',
        ],
        [
            'name' => $page_name,
        ],
    ]);


?>
<h3><?php if ($model_article->id): ?>Редактирование статьи<?php else: ?>Новая статья<?php endif;?></h3>

<?php if ($model_article->getErrors()): ?>
<div class="alert alert-danger"
     role="alert">
    <?=$model_article->getLastErrorMessage()?>
</div>
<?php endif; ?>

<div style="display:none;"
     model="model">
    <?php print_r($model_article); ?>
</div>

<div class="text-left">
    <ul class="nav nav-tabs mb-3"
        id="myTab"
        role="tablist">
        <li class="nav-item">
            <a class="nav-link text-uppercase active"
               id="article-tab"
               data-toggle="tab"
               href="#article"
               role="tab"
               aria-controls="article"
               aria-selected="false">Статья</a>
        </li>


        <?php foreach ($model_article->getAssignedFielsetModelList() as $id_fieldset => $model_anyfield_fielset): ?>
        <li class="nav-item">
            <a class="nav-link text-uppercase "
               id="fieldset<?=$id_fieldset?>-tab"
               data-toggle="tab"
               href="#fieldset<?=$id_fieldset?>"
               role="tab"
               aria-controls="fieldset<?=$id_fieldset?>"
               aria-selected="true">
                <?=$model_anyfield_fielset->name?>
            </a>
        </li>
        <?php endforeach;?>
    </ul>

    <div class="tab-content"
         id="myTabContent">
        <div class="tab-pane fade active show"
             id="article"
             role="tabpanel"
             aria-labelledby="article-tab">


            <?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_form'],
    'form_type'    => 'ajax',
]);?>
            <input type="hidden"
                   name="id_article"
                   value="<?=$model_article->id?>">

            <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'    => 'Категория статьи (id_category)',
            'name'     => 'id_category_list[]',
            'multiple' => '1',
            'options'  => (new \xtetis\xarticle\models\ArticleCategoryModel())->getOptionsCategory(true),

        ],
        'value'      => array_keys($model_article->getArticleCategoryModelList()),
    ]
)?>

            <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя (name)',
            'name'  => 'name',

        ],
        'value'      => $model_article->name,
    ]
)?>


            <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Краткое описание (introtext)',
            'name'  => 'introtext',
            'style' => 'min-height: 150px;height: 110px;',

        ],
        'value'      => $model_article->introtext,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Текст статьи (about)',
            'name'  => 'about',
            'class' => 'tinymce form-control',

        ],
        'value'      => $model_article->about,
    ]
)?>




            <button type="submit"
                    class="btn btn-block btn-primary mb-4">Сохранить</button>
            <?php echo \xtetis\xform\Component::renderFormEnd(); ?>

        </div>
        <?php if ($model_article->id): ?>

        <?php foreach ($model_article->getAssignedFielsetModelList() as $id_fieldset => $model_anyfield_fielset): ?>
        <div class="tab-pane fade "
             id="fieldset<?=$id_fieldset?>"
             role="tabpanel"
             aria-labelledby="fieldset<?=$id_fieldset?>-tab">
            <?=$model_anyfield_fielset->renderForm($model_article->id)?>
        </div>
        <?php endforeach;?>

        <?php endif;?>
    </div>


</div>

<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => '/',
            'name' => 'Админка',
        ],
        [
            'name' => 'Связь категорий с AnyField компонентом',
        ],
    ]);





?>

<a href="/"
   class="btn  btn-primary"
   style="float: right; margin-bottom: 10px;">
    Привязать категорию статей к компоненту AnyField
</a>




<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Категория статей</th>
                    <th>Включая подкатегории</th>
                    <th>Компонент Anyfield</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_article_anyfield_component->model_list as $id => $model): ?>
                <tr>
                    <td><?=$model->id?></td>
                    <td>
                        <?=$model->getModelRelated('id_category')->name?> 
                        (
                            <?=$model->getModelRelated('id_category')->id?> 
                        )
                    </td>
                    <td>
                        <?=$model->include_subcategories?> 
                    </td>
                    <td>
                        <?=$model->getModelRelated('id_anyfield_component')->name?>  
                        / 
                        <?=$model->getModelRelated('id_anyfield_component')->category?>  
                        (
                            <?=$model->getModelRelated('id_anyfield_component')->id?> 
                        )
                    </td>
                    <td>
                    
                    </td>
                </tr>

                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
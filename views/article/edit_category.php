<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_cms_main,
            'name' => 'Админка',
        ],
        [
            'url'  => $url_category_list,
            'name' => 'Категории статей',
        ],
        [
            'name' => 'Редактирование категории',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_validate_form,
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Редактирование категории</h4>
<input type="hidden" name="id" value="<?=$model_article_category->id?>">
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя категории',
            'name'  => 'name',

        ],
        'value'      => $model_article_category->name,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Родительская категория',
            'name'    => 'id_parent',
            'options' => $model_root_article_category->getOptions(0, $model_article_category->id),
        ],
        'value'      => $model_article_category->id_parent,
    ]
)?>
<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


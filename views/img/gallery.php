<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'] ,
            'name' => 'Админка',
        ],
        [
            'name' => 'Изображения - Галереи',
        ],
    ]);
?>

<a href="<?=$urls['url_add_gallery'] ?>"
   class="btn  btn-primary"
   style="float: right; margin-bottom: 10px;">
    Добавить галерею
</a>


<div class="cms_article_list_category_selector">
    <?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_filter_gallery'],
    'form_type'    => 'ajax',
]);?>
    <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Категория галерей',
            'name'    => 'id_category',
            'options' => \xtetis\ximg\models\CategoryModel::getOptions(),
        ],
        'value'      => $id_category,
    ]
)?>
    <button type="submit"
            class="btn btn-block btn-primary mb-4">Фильтровать</button>
    <?=\xtetis\xform\Component::renderFormEnd();?>
</div>


<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_list_params['models'] as $id => $model): ?>
                <tr>
                    <td><?=$model->id?></td>
                    <td><?=$model->name?></td>
                    <td><?=$model->create_date?></td>
                    <td>
                        <a href="<?=$urls['gallery_view'][$id]?>">
                            <i class="far fa-edit"></i>
                        </a>



                        <a href="<?=$urls['gallery_delete'][$id]?>"
                           onclick="return confirm('Удалить ?');">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>

                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<div class="">
    <?=$pagination?>
</div>

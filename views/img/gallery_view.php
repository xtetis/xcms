<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'] ,
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_gallery'] ,
            'name' => 'Галереи',
        ],
        [
            'name' => 'Просмотр галереи',
        ],
    ]);
?>

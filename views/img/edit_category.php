<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_category'],
            'name' => 'Категории',
        ],
        [
            'name' => 'Редкатировать категорию',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_add_category'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Редактирование категории</h4>

<input type="hidden" name="id" value="<?=$model->id?>">

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя категории',
            'name'  => 'name',
        ],
        'value'      => $model->name,
    ]
)?>
<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Изображения - Категории',
        ],
    ]);



?>

<div class="alert alert-primary" role="alert">
  Список категорий галерей (для разных компонентов используются разные категории)
</div>

<a href="<?=$urls['url_add_category']?>"
   class="btn  btn-primary"
   style="float: right; margin-bottom: 10px;">
    Добавить категорию
</a>





<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_category_list as $id => $model_category): ?>
                <tr>
                    <td><?=$model_category->id?></td>
                    <td><?=$model_category->name?></td>
                    <td><?=$model_category->create_date?></td>
                    <td>
                        <a href="<?=$urls['url_edit_category'][$id]?>">
                            <i class="far fa-edit"></i>
                        </a>
                        <a class="cms_article_set_deleted"
                           idx="<?=$id?>"
                           onclick="return confirm('Удалить ?');"
                           href="<?=$urls['url_delete_category'][$id]?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

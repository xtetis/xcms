<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Меню админки',
        ],
    ]);



?>

<div class="alert alert-primary" role="alert">
  Список дополнительных пунктов меню
</div>

<a href="<?=$urls['url_add_menu']?>"
   class="btn  btn-primary"
   style="float: right; margin-bottom: 10px;">
    Добавить пункт меню
</a>





<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Имя</th>
                    <th>Дата</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($list_model_params['models'] as $id => $model_menu): ?>
                <?php if (!intval($model_menu->id_parent)):?>
                <tr>
                    <td><?=$model_menu->id?></td>
                    <td><?=$model_menu->name?></td>
                    <td><?=$model_menu->create_date?></td>
                    <td>
                        <a href="<?=$urls['url_edit'][$id]?>">
                            <i class="far fa-edit"></i>
                        </a>
                        <a class="cms_article_set_deleted"
                           idx="<?=$id?>"
                           onclick="return confirm('Удалить ?');"
                           href="<?=$urls['url_delete'][$id]?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
                    <?php foreach ($list_model_params['models'] as $id_ch => $model_menu_child): ?>
                    <?php if (intval($model_menu_child->id_parent) == $model_menu->id):?>
                    <tr>
                        <td><?=$model_menu_child->id?></td>
                        <td> → <?=$model_menu_child->name?></td>
                        <td><?=$model_menu_child->create_date?></td>
                        <td>
                            <a href="<?=$urls['url_edit'][$id_ch]?>">
                                <i class="far fa-edit"></i>
                            </a>
                            <a class="cms_article_set_deleted"
                            idx="<?=$id_ch?>"
                            onclick="return confirm('Удалить ?');"
                            href="<?=$urls['url_delete'][$id_ch]?>">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    
                    <?php endif; ?>
                    <?php endforeach;?>
                <?php endif; ?>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

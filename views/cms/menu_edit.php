<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'url'  => $urls['url_menu'],
            'name' => 'Меню (админка)',
        ],
        [
            'name' => 'Добавить пункт меню',
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_menu_edit'],
    'form_type'    => 'ajax',
]);?>
<h4 class="mb-3 f-w-400">Добавление пункта меню</h4>

<input type="hidden" name="id" value="<?=$model->id?>">

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Родительский пункт меню',
            'name'    => 'id_parent',
            'options' => (new \xtetis\xcms\models\MenuModel)->getOptionsParent(),

        ],
        'value'      => $model->id_parent,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя пункта меню',
            'name'  => 'name',
        ],
        'value'      => $model->name,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Сожержимое ссылки',
            'name'  => 'content',
            'style' => 'min-height: 150px;height: 110px;',
        ],
        'value'      => $model->content,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Тип ссылки',
            'name'    => 'type',
            'options' => [0 => 'Генерируемая по параметрам', 1 => 'Обрабатываемая (ссылка на PHP файл)'],
        ],
        'value'      => $model->type,
    ]
)?>


<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>


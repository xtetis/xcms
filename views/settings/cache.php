<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Настройки - Кеш',
        ],
    ]);

    $counter = 0;
?>

<div class="alert alert-primary" role="alert">
  Всего файлов кеша: <?=$count_files_cache?>
</div>
<div>
    Время жизни кеша: <?=CACHE_LIFETIME?> сек. (параметр <b>CACHE_LIFETIME</b>)
</div>
<br>

<a class="btn btn-primary" href="<?=$urls['url_flush_cache']?>" role="button">Очистить кеш</a>

<br><br>
<h3>Кеш по тегам</h3>
<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Тег</th>
            <th>Файлов кеша</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($cache_tags as $key => $tag_name): ?>
            <?php
$url_clear_tag = \xtetis\xcms\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'flush_cache',
    ],
    'query'=>[
        'by_tag'=>$tag_name
    ]
]);    
            ?>
            <tr>
                <td>
                    <?=$key?>
                </td>
                <td>
                    <?=$tag_name?>
                </td>
                <td>
                    <?php
                        $count_by_tag = $cache->getTagCacheCount($tag_name);
                        echo $count_by_tag;
                        $counter += $count_by_tag;
                    ?>
                </td>
                <td>
                    <a class="btn btn-primary" href="<?=$url_clear_tag?>" role="button">
                        Очистить для тега
                    </a>

                </td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td colspan="2">
                Итого кеша с тегами
            </td>
            <td>
                <?=$counter?>
            </td>
            <td></td>
        </tr>
    </tbody>
</table>

<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_cms_main,
            'name' => 'Админка',
        ],
        [
            'name' => 'Статьи',
        ],
    ]);

    // Добавляем файл JS для обработки страницы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/cms_article.js');




?>

<a href="<?=$url_add_article?>"
   class="btn  btn-primary"
   style="float: right; margin-bottom: 10px;">
    Добавить статью
</a>

<div class="cms_article_list_category_selector" start_url="<?=$url_start_article_list?>">
<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => '/',
    'form_type'    => 'ajax',
]);?>
<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Категория статей (Показываются все статьи дочерних категорий)',
            'name'    => 'id_parent',
            'options' => $model_root_article_category->getOptions(),
        ],
        'value'      => $id_category,
    ]
)?>
<?=\xtetis\xform\Component::renderFormEnd();?>
</div>




<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Del</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_article_list->article_model_list as $id => $model_article): ?>
                <tr <?=($model_article->deleted) ? ' class="table-danger"' : ''?>>
                    <td><?=$model_article->id?></td>
                    <td><?=$model_article->name?></td>
                    <td><?=$model_article->create_date?></td>
                    <td><?=$model_article->deleted?></td>
                    <td>
                        <a href="<?=$url_article_edit_link_list[$model_article->id]?>">
                            <i class="far fa-edit"></i>
                        </a>


                        <?php if ($model_article->deleted): ?>
                        <a class="cms_article_restore_from_trash"
                           idx="<?=$model_article->id?>"
                           href="<?=$url_article_undelete_link_list[$model_article->id]?>">
                            <i class="fas fa-trash-restore"></i>
                        </a>
                        <?php endif;?>

                        <a class="cms_article_set_deleted"
                           idx="<?=$model_article->id?>"
                           deleted="<?=$model_article->deleted?>"
                           href="<?=$url_article_delete_link_list[$model_article->id]?>">
                            <i class="fas fa-trash"></i>
                        </a>

                    </td>
                </tr>

                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<div class="">
    <?=$pagination?>
</div>
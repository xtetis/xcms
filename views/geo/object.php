<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'ГЕО - Транслит',
        ],
    ]);



?>



<?php if ($only_with_date_ankets):?>
    <a class="btn btn-primary" href="<?=$urls['url_start_list_only_nwda'] ?>" role="button">Все гео объекты</a>
<?php else: ?>
    <a class="btn btn-primary" href="<?=$urls['url_start_list_only_wda'] ?>" role="button">Гео объекты, в которых найдены анкеты</a>
<?php endif;?>
<br>


<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped  table-bordered">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Название</th>
                    <th>sef</th>
                    <th>Тип объекта</th>
                    <th>Родитель</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_list['models'] as $id => $model): ?>
                <tr>
                    <td>
                        <?=$model->id?>
                    </td>
                    <td style="white-space: normal;">
                        <?=$model->name?>
                    </td>
                    <td>
                        <?=$model->sef?>
                    </td>
                    <td>
                        <?=$model->id_geo_type?>
                    </td>
                    <td>
                        <?=$model->id_parent?>
                    </td>
                    <td>
                        <a href="<?=$urls['url_translit'][$id]?>">
                            <i class="fas fa-sync"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

<div class="">
    <?=$pagination?>
</div>
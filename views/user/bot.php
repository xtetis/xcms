<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $urls['url_cms_main'],
            'name' => 'Админка',
        ],
        [
            'name' => 'Пользователи - Боты',
        ],
    ]);



?>

<div class="alert alert-primary" role="alert">
  Тут можно создать нового пользователя Бота.  И в режиме инкогнито зайти под этим ботом в аккаунт.
</div>



<a href="<?=$urls['url_add_bot']?>"
   class="btn  btn-primary"
   style="float: right; margin-bottom: 10px;">
    Добавить бота
</a>





<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>ID пользователя</th>
                    <th>Логин ли имя</th>
                    <th>Хеш доступа</th>
                    <th>Хеш актуален до</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_list['models'] as $id => $model_bot): ?>
                <tr>
                    <td><?=$model_bot->id?></td>
                    <td><?=$model_bot->id_user?></td>
                    <td><?=$model_bot->getModelRelated('id_user')->getUserLoginOrName()?></td>
                    <td>
                        <a href="javascript:void(0)"
                           onclick="alert('<?=$model_bot->getBotLoginLink()?>')"
                           rel="noopener noreferrer">
                            <?=$model_bot->access_hash?>
                        </a>
                    </td>
                    <td><?=$model_bot->hash_actual?></td>
                    <td>
                        <a href="<?=$urls['url_regenerate_access_hash'][$id]?>">
                            <i class="far fa-edit"></i>
                        </a>
                    </td>
                </tr>

                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<div class="">
    <?=$pagination?>
</div>
<?php
// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

?>

<?php foreach ($model_migrarion->component_classes_to_migrate as $component_class) :?>
    <div>
        <div>
            <b>
                <?=$component_class?>
            </b>
        </div>
        <div>
            <?php foreach ($model_migrarion->files_to_migrate[$component_class] as $filename) :?>
                <div>
                <?=$filename?>
                </div>
            <?php endforeach;?>
        </div>
    </div>
<?php endforeach;?>
<?php

namespace xtetis\xcms;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 * Компонент CMS
 */
class Component extends \xtetis\xengine\models\Component
{

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {
        parent::__construct($params);

        // Валидирует параметры, необходимые для работы движка
        \xtetis\xcms\Config::validateParams();

        \xtetis\xengine\App::getApp()->setParam('layout', 'manager');
    }



    /**
     * Список роутов для компонента xdate
     */
    public static function getComponentRoutes()
    {
        $routes = [

            // https://lsfinder.com/manager/component_admin?cname=diary&aname=admin&qname=community
            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')\/component_admin$/',
                'function' => function (
                    $params = []
                )
                {
                    
                    \xtetis\xengine\App::getApp()->component_name = \xtetis\xengine\helpers\RequestHelper::get('cname','str','');
                    \xtetis\xengine\App::getApp()->action_name    = \xtetis\xengine\helpers\RequestHelper::get('aname','str','');
                    \xtetis\xengine\App::getApp()->query_name     = \xtetis\xengine\helpers\RequestHelper::get('qname','str','');
                    
                    if (\xtetis\xengine\App::getApp()->action_name == 'admin')
                    {
                        // Если этоадминский роут - то определяем константу ADMIN, чтобы извне нельзя было вызвать
                        define('ADMIN',TRUE);
                    }
                    \xtetis\xengine\App::getApp()->setParam('layout', 'manager');

                    \xtetis\xengine\App::getApp()->runComponent(['required_constants'=>['ADMIN']]);
                    
                },
            ],

            // https://lsfinder.com/date/profile/index/15
            [
                'url'      => '/^\/' . self::getComponentUrl() . '\/profile\/index\/(\d+)$/',
                'function' => function (
                    $params = []
                )
                {
                    $id = isset($params[0]) ? $params[0] : '';
                    $id = intval($id);

                    $model = \xtetis\xdate\models\ProfileModel::generateModelById($id);
                    if ($model)
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: ' . $model->getLink());
                        exit();

                    }
                    else
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: /' . self::getComponentUrl());
                        exit();
                    }
                },
            ],

            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')(\/(\w+)?(\/(\d+)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = 'index';

                    $id = isset($params[4]) ? $params[4] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }
                },
            ],
            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')(\/(\w+)(\/(\w+)(\/(\d+)(\/(\d+)?)??)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = isset($params[4]) ? $params[4] : '';

                    $id  = isset($params[6]) ? $params[6] : '';
                    $id2 = isset($params[8]) ? $params[8] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }

                    if (strlen($id2))
                    {
                        $_GET['id2'] = $id2;
                    }
                },
            ],
        ];

        return $routes;
    }

    /**
     * Возвращает массив со ссылками для меню админки
     */
    public static function getCmsLinks()
    {

        $menu = [
            [
                'name'  => 'Админка',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl(),
                        'name' => 'Админка',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'cms',
                                'menu',
                            ],
                        ]),
                        'name' => 'Меню',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'seo',
                                'index',
                            ],
                        ]),
                        'name' => 'SEO',
                    ],
                ],
            ],

            [
                'name'  => 'Статьи',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'article',
                                'category_list',
                            ],
                        ]),
                        'name' => 'Категории статей',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'article',
                                'article_list',
                            ],
                        ]),
                        'name' => 'Статьи',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'article',
                                'category_in_anyfield_component_list',
                            ],
                        ]),
                        'name' => 'Связь категорий с AnyField компонентом',
                    ],
                ],
            ],
            [
                'name'  => 'Дополнительные поля',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'anyfield',
                                'category_list',
                            ],
                        ]),
                        'name' => 'Категории статей',
                    ],
                ],
            ],

            [
                'name'  => 'RBAC',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'rbac',
                                'resource_list',
                            ],
                        ]),
                        'name' => 'Ресурсы',
                    ],
                ],
            ],

            [
                'name'  => 'Знакомства',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'date',
                                'party',
                            ],
                        ]),
                        'name' => 'Вечеринки',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'date',
                                'gen_anket',
                            ],
                        ]),
                        'name' => 'Генерация анкет',
                    ],
                ],
            ],

            [
                'name'  => 'Пользователи',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'user',
                                'bot',
                            ],
                        ]),
                        'name' => 'Боты',
                    ],
                ],
            ],
            [
                'name'  => 'Форум',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'forum',
                                'group',
                            ],
                        ]),
                        'name' => 'Группы',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'forum',
                                'theme',
                            ],
                        ]),
                        'name' => 'Темы',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'forum',
                                'message',
                            ],
                        ]),
                        'name' => 'Сообщения',
                    ],
                ],
            ],

            [
                'name'  => 'Изображения',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'img',
                                'category',
                            ],
                        ]),
                        'name' => 'Категории',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'img',
                                'gallery',
                            ],
                        ]),
                        'name' => 'Галереи',
                    ],
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'gallery',
                            ],
                        ]),
                        'name' => 'Галереи (old)',
                    ],
                ],
            ],

            [
                'name'  => 'Гео',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'geo',
                                'object',
                            ],
                        ]),
                        'name' => 'Объекты',
                    ],
                ],
            ],

            [
                'name' => 'Миграции',
                'url'  => \xtetis\xcms\Component::makeUrl([
                    'path' => [
                        'migration',
                        'index',
                    ],
                ]),
            ],

            [
                'name'  => 'Настройки',
                'child' => [
                    [
                        'url'  => \xtetis\xcms\Component::makeUrl([
                            'path' => [
                                'settings',
                                'cache',
                            ],
                        ]),
                        'name' => 'Кеш',
                    ],
                ],
            ],
        ];

        $user_menu         = [];
        $list_model_params = \xtetis\xcms\models\MenuModel::getListModelsParams([
            'limit' => 999999,
        ]);

        foreach ($list_model_params['models'] as $id => $model)
        {
            if (!intval($model->id_parent))
            {
                $item = [
                    'name' => $model->name,
                ];

                $child = [];
                foreach ($list_model_params['models'] as $id_ch => $model_ch)
                {
                    if (intval($model->id) == $model_ch->id_parent)
                    {
                        $child[] = [
                            'url'  => \xtetis\xcms\Component::makeUrl([
                                'path'  => [
                                    'cms',
                                    'menu_prepare',
                                ],
                                'query' => [
                                    'id' => $model_ch->id,
                                ],
                            ]),
                            'name' => $model_ch->name,
                        ];

                    }
                }

                if ($child)
                {
                    $item['child'] = $child;
                }

                $user_menu[] = $item;
            }
        }

        $menu = array_merge($menu, $user_menu);


        // Получаем список ссылок для каждого подключенного компонента
        // Это более правильный вариант

        // Получаем сначала список подключенных компонентов
        $component_url_list = unserialize(COMPONENT_URL_LIST);
        $component_menu     = [];
        foreach ($component_url_list as $component_url => $component_class)
        {
            $manager_actions = $component_class::getManagerActions();
            $childs          = [];
            foreach ($manager_actions as $manager_action)
            {
                $childs[] = [
                    'url'  => \xtetis\xcms\Component::makeUrl([
                        'path' => [
                            'component_admin',
                        ],
                        'query'=>[
                            'cname'=>$component_url,
                            'aname'=>$manager_action['action'],
                            'qname'=>$manager_action['query'],
                        ]
                    ]),
                    'name' => $manager_action['title'],
                ];
            }

            $item = [
                'name'  => $component_url,
                'child' => $childs,
            ];

            if ($childs)
            {
                $component_menu[] = $item;
            }

        }

        $menu = array_merge($menu, $component_menu);

        return $menu;
    }

    /**
     * Страница кастомной ошибки
     */
    public static function cmsCustomDie($message)
    {
        \xtetis\xpagemessage\Component::setPageMessageParams([
            'message' => $message,
            'name'    => 'Ошибка',
            'title'   => 'Ошибка',
        ]);
        \xtetis\xpagemessage\Component::renderPageMessage();
        exit;
    }

    /**
     * Возвращает урл конпонента, который открывается через админку
     */
    public static function makeUrlAdminComponent(
        $component_class='',
        $component_action = '',
        $component_query = '',
        $add_query = [])
    {

        $query = $add_query;
        $component_url_list = \xtetis\xengine\Config::getComponentUrlList();

        foreach ($component_url_list as $c_url => $c_class) 
        {
            if ($component_class == $c_class)
            {
                $query['cname'] = $c_url;
                break;
            }
        }

        $query['aname']=$component_action;
        $query['qname']=$component_query;

        return \xtetis\xcms\Component::makeUrl([
            'path' => [
                'component_admin',
            ],
            'query'=>$query
        ]);
    }


}
